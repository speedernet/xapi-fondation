var BaseEnum = (function () {
    function BaseEnum(str) {
        this.value = str;
    }
    return BaseEnum;
})();
///<reference path="../baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var RequestMethod = (function (_super) {
    __extends(RequestMethod, _super);
    function RequestMethod() {
        _super.apply(this, arguments);
    }
    RequestMethod.PUT = "PUT";
    RequestMethod.POST = "POST";
    RequestMethod.GET = "GET";
    RequestMethod.DELETE = "DELETE";
    return RequestMethod;
})(BaseEnum);
///<reference path="baseEnum.ts"/>
var ObjectType = (function (_super) {
    __extends(ObjectType, _super);
    function ObjectType() {
        _super.apply(this, arguments);
    }
    ObjectType.GROUP = 'Group';
    ObjectType.AGENT = "Agent";
    ObjectType.ACTIVITY = "Activity";
    ObjectType.STATEMENT = "Statement";
    ObjectType.STATEMENT_REF = "StatementRef";
    ObjectType.SUB_STATEMENT = "SubStatement";
    return ObjectType;
})(BaseEnum);
///<reference path="../enums/objectType.ts"/>
///<reference path="IBaseObj.ts"/>
var DictionaryMapper = (function () {
    function DictionaryMapper(name) {
        this.key = name;
        this.bit = DictionaryMapper.currentBit++;
    }
    DictionaryMapper.currentBit = 0;
    return DictionaryMapper;
})();
var Dictionary = (function () {
    function Dictionary() {
        this.items = Array();
        this.mappers = Array();
    }
    Dictionary.prototype.add = function (key, value) {
        var mapper = new DictionaryMapper(key);
        this.items[mapper.bit] = value;
        this.mappers.push(mapper);
    };
    Dictionary.prototype.get = function (key) {
        var i = this.searchFor(key);
        return this.items[this.mappers[i].bit];
    };
    Dictionary.prototype.getKeys = function () {
        var keys = Array();
        for (var i = 0; i < this.mappers.length; i++) {
            keys.push(this.mappers[i].key);
        }
        return keys;
    };
    Dictionary.prototype.getValues = function () {
        return this.items;
    };
    Dictionary.prototype.update = function (key, value) {
        var i = this.searchFor(key);
        this.items[this.mappers[i].bit] = value;
    };
    Dictionary.prototype.remove = function (key) {
        var i = this.searchFor(key);
        if (i == this.mappers.length - 1) {
            this.items.pop();
            this.mappers.pop();
        }
        else {
            this.items.splice(this.mappers[i].bit, 1);
            this.mappers.splice(i, 1);
        }
    };
    Dictionary.prototype.iterate = function (iterator) {
        if (iterator)
            this.setIterator(iterator);
        for (var i = 0; i < this.mappers.length; i++) {
            this.iterator(this.mappers[i].bit, this.items[this.mappers[i].bit], this.items);
        }
    };
    Dictionary.prototype.setIterator = function (iterator) {
        this.iterator = iterator;
    };
    Dictionary.prototype.exists = function (key) {
        for (var i = 0; i < this.mappers.length; i++) {
            if (key == this.mappers[i].key)
                return true;
        }
        return false;
    };
    Dictionary.prototype.count = function () {
        return this.mappers.length;
    };
    Dictionary.prototype.searchFor = function (key) {
        for (var i = 0; i < this.mappers.length; i++) {
            if (key == this.mappers[i].key)
                return i;
        }
        throw new Error('Dictionary element "' + key + '" not found');
    };
    return Dictionary;
})();
///<reference path="dictionary.ts"/>
var LangMap = (function (_super) {
    __extends(LangMap, _super);
    function LangMap() {
        _super.apply(this, arguments);
    }
    return LangMap;
})(Dictionary);
///<reference path="baseEnum.ts"/>
var BaseVerb = (function (_super) {
    __extends(BaseVerb, _super);
    function BaseVerb() {
        _super.apply(this, arguments);
    }
    /** @desc: simple stuff */
    BaseVerb.ATTEMPTED = "http://adlnet.gov/expapi/verbs/attempted";
    BaseVerb.EXPERIENCED = "http://adlnet.gov/expapi/verbs/experienced";
    BaseVerb.PROGRESSED = "http://adlnet.gov/expapi/verbs/progressed";
    BaseVerb.INTERACTED = "http://adlnet.gov/expapi/verbs/interacted";
    BaseVerb.COMPLETED = "http://adlnet.gov/expapi/verbs/completed";
    /** @desc: session management */
    BaseVerb.INITIALIZED = "http://adlnet.gov/expapi/verbs/initialized";
    BaseVerb.RESUMED = "http://adlnet.gov/expapi/verbs/resumed";
    BaseVerb.SUSPENDED = "http://adlnet.gov/expapi/verbs/suspended";
    BaseVerb.EXITED = "http://adlnet.gov/expapi/verbs/exited";
    BaseVerb.TERMINATED = "http://adlnet.gov/expapi/verbs/terminated";
    /** @desc: navigation */
    BaseVerb.LAUNCHED = "http://adlnet.gov/expapi/verbs/launched";
    /** @desc: quiz */
    BaseVerb.RESPONDED = "http://adlnet.gov/expapi/verbs/responded";
    BaseVerb.PASSED = "http://adlnet.gov/expapi/verbs/passed";
    BaseVerb.SCORED = "http://adlnet.gov/expapi/verbs/scored";
    BaseVerb.FAILED = "http://adlnet.gov/expapi/verbs/failed";
    /** @desc: social */
    BaseVerb.ASKED = "http://adlnet.gov/expapi/verbs/asked";
    BaseVerb.COMMENTED = "http://adlnet.gov/expapi/verbs/commented";
    BaseVerb.ANSWERED = "http://adlnet.gov/expapi/verbs/aswered";
    BaseVerb.SHARED = "http://adlnet.gov/expapi/verbs/shared";
    /** @desc: blended */
    BaseVerb.REGISTERED = "http://adlnet.gov/expapi/verbs/registered";
    BaseVerb.ATTENDED = "http://adlnet.gov/expapi/verbs/attended";
    /** @desc: skills */
    BaseVerb.MASTERED = "http://adlnet.gov/expapi/verbs/mastered";
    /** @desc: other */
    BaseVerb.IMPORTED = "http://adlnet.gov/expapi/verbs/imported";
    BaseVerb.PREFERRED = "http://adlnet.gov/expapi/verbs/preferred";
    BaseVerb.VOIDED = "http://adlnet.gov/expapi/verbs/voided";
    return BaseVerb;
})(BaseEnum);
///<reference path="../utils/langMap.ts"/>
///<reference path="../enums/baseVerb.ts"/>
///<reference path="../../utils/langMap.ts"/>
///<reference path="../IObj.ts"/>
var Activity = (function () {
    function Activity() {
        this.definition = null;
        /** @desc: optional */
        this.objectType = new ObjectType(ObjectType.ACTIVITY);
    }
    return Activity;
})();
///<reference path="../IActor.ts"/>
var Agent = (function () {
    function Agent() {
        /** @desc: optional */
        this.objectType = new ObjectType(ObjectType.AGENT);
        this.name = null;
    }
    return Agent;
})();
///<reference path="../IActor.ts"/>
///<reference path="agent.ts"/>
var Group = (function () {
    function Group() {
        this.objectType = new ObjectType(ObjectType.GROUP);
        /** @desc: optional */
        this.name = null;
        this.member = new Array();
    }
    return Group;
})();
///<reference path="../IObj.ts"/>
var StatementRef = (function () {
    function StatementRef() {
        this.objectType = new ObjectType(ObjectType.STATEMENT_REF);
    }
    return StatementRef;
})();
///<reference path="IActor.ts"/>
///<reference path="impl/group.ts"/>
///<reference path="impl/statementRef.ts"/>
///<reference path="impl/activity.ts"/>
///<reference path="../utils/langMap.ts"/>
///<reference path="../IBaseObj.ts"/>
///<reference path="../IActor.ts"/>
///<reference path="../IVerb.ts"/>
///<reference path="activity.ts"/>
var SubStatement = (function () {
    function SubStatement() {
        this.objectType = new ObjectType(ObjectType.SUB_STATEMENT);
        this.result = null;
        this.context = null;
    }
    return SubStatement;
})();
///<reference path="IActor.ts"/>
///<reference path="IVerb.ts"/>
///<reference path="impl/Activity.ts"/>
///<reference path="IResult.ts"/>
///<reference path="impl/agent.ts"/>
///<reference path="IContext.ts"/>
///<reference path="IAttachment.ts"/>
///<reference path="impl/statementRef.ts"/>
///<reference path="impl/subStatement.ts"/>
///<reference path="../enums/requestEnums/requestMethod.ts"/>
///<reference path="../entities/IStatement.ts"/>
var BaseRequester // note: should be abstract
 = (function () {
    function BaseRequester // note: should be abstract
        (uri) {
        var _this = this;
        if (uri)
            this.endpoint = uri;
        this.socket = new XMLHttpRequest();
        this.socket.onreadystatechange = function (event) { return _this.asyncReturn(event); };
    }
    BaseRequester // note: should be abstract
    .prototype.put = function (stuff) {
        this.request(RequestMethod.PUT, stuff);
    };
    BaseRequester // note: should be abstract
    .prototype.post = function (stuff) {
        this.request(RequestMethod.POST, stuff);
    };
    BaseRequester // note: should be abstract
    .prototype.get = function (search) {
        this.request(RequestMethod.GET, search);
    };
    BaseRequester // note: should be abstract
    .prototype.result = function (ajaxResponse) {
        // TODO for request treatment, add your own callbacks
        var result = JSON.parse(ajaxResponse);
        console.error('Abstract method, implement me');
    };
    BaseRequester // note: should be abstract
    .prototype.handleSocketError = function (errCode) {
        console.error('Abstract method, implement me');
    };
    BaseRequester // note: should be abstract
    .prototype.request = function (mode, params) {
        if (!this.endpoint)
            throw new Error('No webservice url set');
        this.socket.open(mode, this.endpoint, true);
        this.socket.send(params);
    };
    BaseRequester // note: should be abstract
    .prototype.asyncReturn = function (event) {
        if (4 == this.socket.readyState && this.socket.response) {
            if (200 == this.socket.status)
                this.result(this.socket.response);
            else
                this.handleSocketError(this.socket.status);
        }
    };
    return BaseRequester // note: should be abstract
    ;
})();
///<reference path="baseReq.ts"/>
var BaseSemaphoreRequester = (function (_super) {
    __extends(BaseSemaphoreRequester, _super);
    function BaseSemaphoreRequester() {
        _super.apply(this, arguments);
        // TODO: you must update ETag on get result
        /** @desc: a hash received from LRS to identify data version */
        this.ETag = '';
    }
    BaseSemaphoreRequester.prototype.put = function (stuff) {
        var oldTag = this.ETag;
        this.get(stuff);
        if (oldTag == this.ETag)
            _super.prototype.put.call(this, stuff);
    };
    BaseSemaphoreRequester.prototype.post = function (stuff) {
        console.warn('No transactional control on post method');
        _super.prototype.post.call(this, stuff);
    };
    return BaseSemaphoreRequester;
})(BaseRequester);
///<reference path="requesters/baseSemaphoreReq.ts"/>
///<reference path="entities/IStatement.ts"/>
var requester = new BaseSemaphoreRequester('http://localhost/xapitest/');
requester.get({});
alert('Hello! This is a dummy test');
