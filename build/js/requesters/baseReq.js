///<reference path="../enums/requestEnums/requestMethod.ts"/>
///<reference path="../entities/IStatement.ts"/>
var BaseRequester // note: should be abstract
 = (function () {
    function BaseRequester // note: should be abstract
        (uri) {
        var _this = this;
        if (uri)
            this.endpoint = uri;
        this.socket = new XMLHttpRequest();
        this.socket.onreadystatechange = function (event) { return _this.asyncReturn(event); };
    }
    BaseRequester // note: should be abstract
    .prototype.put = function (stuff) {
        this.request(RequestMethod.PUT, stuff);
    };
    BaseRequester // note: should be abstract
    .prototype.post = function (stuff) {
        this.request(RequestMethod.POST, stuff);
    };
    BaseRequester // note: should be abstract
    .prototype.get = function (search) {
        this.request(RequestMethod.GET, search);
    };
    BaseRequester // note: should be abstract
    .prototype.result = function (ajaxResponse) {
        // TODO for request treatment, add your own callbacks
        var result = JSON.parse(ajaxResponse);
        console.error('Abstract method, implement me');
    };
    BaseRequester // note: should be abstract
    .prototype.handleSocketError = function (errCode) {
        console.error('Abstract method, implement me');
    };
    BaseRequester // note: should be abstract
    .prototype.request = function (mode, params) {
        if (!this.endpoint)
            throw new Error('No webservice url set');
        this.socket.open(mode, this.endpoint, true);
        this.socket.send(params);
    };
    BaseRequester // note: should be abstract
    .prototype.asyncReturn = function (event) {
        if (4 == this.socket.readyState && this.socket.response) {
            if (200 == this.socket.status)
                this.result(this.socket.response);
            else
                this.handleSocketError(this.socket.status);
        }
    };
    return BaseRequester // note: should be abstract
    ;
})();
