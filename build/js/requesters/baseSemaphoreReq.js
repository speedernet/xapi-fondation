///<reference path="baseReq.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var BaseSemaphoreRequester = (function (_super) {
    __extends(BaseSemaphoreRequester, _super);
    function BaseSemaphoreRequester() {
        _super.apply(this, arguments);
        // TODO: you must update ETag on get result
        /** @desc: a hash received from LRS to identify data version */
        this.ETag = '';
    }
    BaseSemaphoreRequester.prototype.put = function (stuff) {
        var oldTag = this.ETag;
        this.get(stuff);
        if (oldTag == this.ETag)
            _super.prototype.put.call(this, stuff);
    };
    BaseSemaphoreRequester.prototype.post = function (stuff) {
        console.warn('No transactional control on post method');
        _super.prototype.post.call(this, stuff);
    };
    return BaseSemaphoreRequester;
})(BaseRequester);
