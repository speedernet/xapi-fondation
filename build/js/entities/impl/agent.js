///<reference path="../IActor.ts"/>
var Agent = (function () {
    function Agent() {
        /** @desc: optional */
        this.objectType = new ObjectType(ObjectType.AGENT);
        this.name = null;
    }
    return Agent;
})();
