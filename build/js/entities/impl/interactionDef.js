///<reference path="../IInteractionDef.ts"/>
/** @desc: for true/false, fill-in, long-fill-in, number, other */
var SimpleInteractionDef = (function () {
    function SimpleInteractionDef(type) {
        this.interactionType = type;
    }
    return SimpleInteractionDef;
})();
var QCMInteractionDef = (function () {
    function QCMInteractionDef() {
        this.interactionType = new InteractionType(InteractionType.CHOICE);
        this.choices = new Array();
    }
    return QCMInteractionDef;
})();
var ScaleInteractionDef = (function () {
    function ScaleInteractionDef() {
        this.interactionType = new InteractionType(InteractionType.LIKERT);
        this.scale = new Array();
    }
    return ScaleInteractionDef;
})();
var MatchInteractionDef = (function () {
    function MatchInteractionDef() {
        this.interactionType = new InteractionType(InteractionType.MATCHING);
        this.source = new Array();
        this.target = new Array();
    }
    return MatchInteractionDef;
})();
var PerformanceInteractionDef = (function () {
    function PerformanceInteractionDef() {
        this.interactionType = new InteractionType(InteractionType.PERFORMANCE);
        this.steps = new Array();
    }
    return PerformanceInteractionDef;
})();
var SequencingInteractionDef = (function () {
    function SequencingInteractionDef() {
        this.interactionType = new InteractionType(InteractionType.SEQUENCING);
        this.choices = new Array();
    }
    return SequencingInteractionDef;
})();
