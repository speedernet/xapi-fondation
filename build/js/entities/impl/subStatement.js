///<reference path="../IBaseObj.ts"/>
///<reference path="../IActor.ts"/>
///<reference path="../IVerb.ts"/>
///<reference path="activity.ts"/>
var SubStatement = (function () {
    function SubStatement() {
        this.objectType = new ObjectType(ObjectType.SUB_STATEMENT);
        this.result = null;
        this.context = null;
    }
    return SubStatement;
})();
