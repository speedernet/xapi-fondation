///<reference path="../../utils/langMap.ts"/>
///<reference path="../IObj.ts"/>
var Activity = (function () {
    function Activity() {
        this.definition = null;
        /** @desc: optional */
        this.objectType = new ObjectType(ObjectType.ACTIVITY);
    }
    return Activity;
})();
