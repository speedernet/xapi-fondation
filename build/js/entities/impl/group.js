///<reference path="../IActor.ts"/>
///<reference path="agent.ts"/>
var Group = (function () {
    function Group() {
        this.objectType = new ObjectType(ObjectType.GROUP);
        /** @desc: optional */
        this.name = null;
        this.member = new Array();
    }
    return Group;
})();
