///<reference path="baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var ActivityType = (function (_super) {
    __extends(ActivityType, _super);
    function ActivityType() {
        _super.apply(this, arguments);
    }
    // SCORM-like
    ActivityType.COURSE = "course";
    ActivityType.MODULE = "module";
    ActivityType.LESSON = "lesson";
    ActivityType.INTERACTION = "interaction";
    ActivityType.OBJECTIVE = "objective";
    ActivityType.ATTEMPT = "attempt";
    // xAPI bonuses
    ActivityType.FILE = "file";
    ActivityType.LINK = "link";
    ActivityType.MEDIA = "media";
    ActivityType.MEETING = "meeting";
    ActivityType.ASSESSMENT = "assessment";
    ActivityType.PERFORMANCE = "performance";
    ActivityType.PROFILE = "profile";
    ActivityType.QUESTION = "question";
    ActivityType.SIMULATION = "simulation";
    return ActivityType;
})(BaseEnum);
