///<reference path="baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var InteractionType = (function (_super) {
    __extends(InteractionType, _super);
    function InteractionType() {
        _super.apply(this, arguments);
    }
    InteractionType.TRUE_FALSE = "true-false";
    InteractionType.CHOICE = "choice"; // multiple-choice question (checkboxes)
    InteractionType.FILL_IN = "fill-in"; // blanked-out text
    InteractionType.LONG_FILL_IN = "long-fill-in"; // any text
    InteractionType.LIKERT = "likert"; // scale elements
    InteractionType.MATCHING = "matching"; // association of elements
    InteractionType.PERFORMANCE = "performance"; // procedure
    InteractionType.SEQUENCING = "sequencing"; // ranking
    InteractionType.NUMERIC = "numeric";
    InteractionType.OTHER = "other";
    return InteractionType;
})(BaseEnum);
