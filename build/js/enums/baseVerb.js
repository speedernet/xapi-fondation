///<reference path="baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var BaseVerb = (function (_super) {
    __extends(BaseVerb, _super);
    function BaseVerb() {
        _super.apply(this, arguments);
    }
    /** @desc: simple stuff */
    BaseVerb.ATTEMPTED = "http://adlnet.gov/expapi/verbs/attempted";
    BaseVerb.EXPERIENCED = "http://adlnet.gov/expapi/verbs/experienced";
    BaseVerb.PROGRESSED = "http://adlnet.gov/expapi/verbs/progressed";
    BaseVerb.INTERACTED = "http://adlnet.gov/expapi/verbs/interacted";
    BaseVerb.COMPLETED = "http://adlnet.gov/expapi/verbs/completed";
    /** @desc: session management */
    BaseVerb.INITIALIZED = "http://adlnet.gov/expapi/verbs/initialized";
    BaseVerb.RESUMED = "http://adlnet.gov/expapi/verbs/resumed";
    BaseVerb.SUSPENDED = "http://adlnet.gov/expapi/verbs/suspended";
    BaseVerb.EXITED = "http://adlnet.gov/expapi/verbs/exited";
    BaseVerb.TERMINATED = "http://adlnet.gov/expapi/verbs/terminated";
    /** @desc: navigation */
    BaseVerb.LAUNCHED = "http://adlnet.gov/expapi/verbs/launched";
    /** @desc: quiz */
    BaseVerb.RESPONDED = "http://adlnet.gov/expapi/verbs/responded";
    BaseVerb.PASSED = "http://adlnet.gov/expapi/verbs/passed";
    BaseVerb.SCORED = "http://adlnet.gov/expapi/verbs/scored";
    BaseVerb.FAILED = "http://adlnet.gov/expapi/verbs/failed";
    /** @desc: social */
    BaseVerb.ASKED = "http://adlnet.gov/expapi/verbs/asked";
    BaseVerb.COMMENTED = "http://adlnet.gov/expapi/verbs/commented";
    BaseVerb.ANSWERED = "http://adlnet.gov/expapi/verbs/aswered";
    BaseVerb.SHARED = "http://adlnet.gov/expapi/verbs/shared";
    /** @desc: blended */
    BaseVerb.REGISTERED = "http://adlnet.gov/expapi/verbs/registered";
    BaseVerb.ATTENDED = "http://adlnet.gov/expapi/verbs/attended";
    /** @desc: skills */
    BaseVerb.MASTERED = "http://adlnet.gov/expapi/verbs/mastered";
    /** @desc: other */
    BaseVerb.IMPORTED = "http://adlnet.gov/expapi/verbs/imported";
    BaseVerb.PREFERRED = "http://adlnet.gov/expapi/verbs/preferred";
    BaseVerb.VOIDED = "http://adlnet.gov/expapi/verbs/voided";
    return BaseVerb;
})(BaseEnum);
