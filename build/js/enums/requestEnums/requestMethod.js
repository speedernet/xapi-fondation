///<reference path="../baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var RequestMethod = (function (_super) {
    __extends(RequestMethod, _super);
    function RequestMethod() {
        _super.apply(this, arguments);
    }
    RequestMethod.PUT = "PUT";
    RequestMethod.POST = "POST";
    RequestMethod.GET = "GET";
    RequestMethod.DELETE = "DELETE";
    return RequestMethod;
})(BaseEnum);
