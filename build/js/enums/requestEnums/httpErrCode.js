///<reference path="../baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var RestErrCode = (function (_super) {
    __extends(RestErrCode, _super);
    function RestErrCode() {
        _super.apply(this, arguments);
    }
    RestErrCode[400] = 400; // Bad Request
    RestErrCode[401] = 401; // Unauthorized
    RestErrCode[403] = 403; // Forbidden
    RestErrCode[404] = 404; // Not Found
    RestErrCode[409] = 409; // Conflict
    RestErrCode[412] = 412; // Precondition Failed
    RestErrCode[413] = 413; // Request Entity Too Large
    RestErrCode[500] = 500; // Internal Server Error
    return RestErrCode;
})(BaseEnum);
