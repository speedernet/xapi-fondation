///<reference path="baseEnum.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var ObjectType = (function (_super) {
    __extends(ObjectType, _super);
    function ObjectType() {
        _super.apply(this, arguments);
    }
    ObjectType.GROUP = 'Group';
    ObjectType.AGENT = "Agent";
    ObjectType.ACTIVITY = "Activity";
    ObjectType.STATEMENT = "Statement";
    ObjectType.STATEMENT_REF = "StatementRef";
    ObjectType.SUB_STATEMENT = "SubStatement";
    return ObjectType;
})(BaseEnum);
