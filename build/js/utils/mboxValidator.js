var MBox = (function () {
    function MBox() {
    }
    MBox.validate = function (str) {
        var rx = new RegExp('^mailto:[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$');
        var results = str.match(rx);
        return null == results;
    };
    return MBox;
})();
