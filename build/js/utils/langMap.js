///<reference path="dictionary.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LangMap = (function (_super) {
    __extends(LangMap, _super);
    function LangMap() {
        _super.apply(this, arguments);
    }
    return LangMap;
})(Dictionary);
