var DictionaryMapper = (function () {
    function DictionaryMapper(name) {
        this.key = name;
        this.bit = DictionaryMapper.currentBit++;
    }
    DictionaryMapper.currentBit = 0;
    return DictionaryMapper;
})();
var Dictionary = (function () {
    function Dictionary() {
        this.items = Array();
        this.mappers = Array();
    }
    Dictionary.prototype.add = function (key, value) {
        var mapper = new DictionaryMapper(key);
        this.items[mapper.bit] = value;
        this.mappers.push(mapper);
    };
    Dictionary.prototype.get = function (key) {
        var i = this.searchFor(key);
        return this.items[this.mappers[i].bit];
    };
    Dictionary.prototype.getKeys = function () {
        var keys = Array();
        for (var i = 0; i < this.mappers.length; i++) {
            keys.push(this.mappers[i].key);
        }
        return keys;
    };
    Dictionary.prototype.getValues = function () {
        return this.items;
    };
    Dictionary.prototype.update = function (key, value) {
        var i = this.searchFor(key);
        this.items[this.mappers[i].bit] = value;
    };
    Dictionary.prototype.remove = function (key) {
        var i = this.searchFor(key);
        if (i == this.mappers.length - 1) {
            this.items.pop();
            this.mappers.pop();
        }
        else {
            this.items.splice(this.mappers[i].bit, 1);
            this.mappers.splice(i, 1);
        }
    };
    Dictionary.prototype.iterate = function (iterator) {
        if (iterator)
            this.setIterator(iterator);
        for (var i = 0; i < this.mappers.length; i++) {
            this.iterator(this.mappers[i].bit, this.items[this.mappers[i].bit], this.items);
        }
    };
    Dictionary.prototype.setIterator = function (iterator) {
        this.iterator = iterator;
    };
    Dictionary.prototype.exists = function (key) {
        for (var i = 0; i < this.mappers.length; i++) {
            if (key == this.mappers[i].key)
                return true;
        }
        return false;
    };
    Dictionary.prototype.count = function () {
        return this.mappers.length;
    };
    Dictionary.prototype.searchFor = function (key) {
        for (var i = 0; i < this.mappers.length; i++) {
            if (key == this.mappers[i].key)
                return i;
        }
        throw new Error('Dictionary element "' + key + '" not found');
    };
    return Dictionary;
})();
