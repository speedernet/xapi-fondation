#!/bin/bash
echo "TypeScript Compilation"
echo "----------------------"
cd `dirname $0`
echo " -> listing TypeScript classes..."
find . -name \*.ts -print > ts-files.txt
echo "     OK"
echo " -> compiling every js file..."
rm -r ./build/js/
tsc @ts-files.txt --outDir ./build/js/
echo "     OK"
echo " -> merging global js file..."
tsc ./src/main.ts --out ./build/main.js
echo "     OK"
echo " -> minification..."

# main all
uglifyjs    ./build/main.js \
            -o ./build/main.min.js -c -m 'sort' -e

echo "     OK"
echo "----------------------"
echo "     End"