
class MBox
{
	public static validate(str: string): boolean
	{
		var rx: RegExp = new RegExp('^mailto:[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$');
		var results: any = str.match(rx);

		return null == results;
	}
}