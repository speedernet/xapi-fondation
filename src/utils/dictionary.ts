
class DictionaryMapper
{
    public key: string;
    public bit: number;
    private static currentBit: number = 0;

    public constructor(name: string)
    {
        this.key = name;
        this.bit = DictionaryMapper.currentBit++;
    }
}

class Dictionary<T>
{
    private items: T[] = Array<T>();
    private mappers: DictionaryMapper[] = Array<DictionaryMapper>();
    private iterator: (index: number, currentIteration: T, iteratedObj: T[]) => void;
	private _currentKey: string = '';
	private _currentIndex: number = 0;
    public constructor() {}

    public add(key: string, value: T): void
    {
        var mapper: DictionaryMapper = new DictionaryMapper(key);
        this.items[ mapper.bit ] = value;
        this.mappers.push(mapper);
		this._currentIndex = this._mappers.length - 1;
		this._currentKey = key;
    }

    public get(key: string): T
    {
        var i = this.searchFor(key);
        return this.items[ this.mappers[i].bit ];
    }

    public get keys(): string[]
    {
        var keys: string[] = Array<string>(),
            len = this.mappers.length;
        for (var i = 0; i < len; i++)
        {
            keys.push(this.mappers[i].key);
        }

        return keys;
    }

    public get values(): T[]
    {
        var values: T[] = Array<T>(),
			len = this.mappers.length;

		for (var i = 0; i < len; i++)
		{
			values.push(this.items[ this.mappers[i].bit ]);
		}

		return values;
    }

	public get currentIndex(): number
	{
		return this._currentIndex;
	}

	public get currentKey(): string
	{
		return this._currentKey;
	}

	public get length(): number
	{
		return this.mappers.length;
	}

    public update(key: string, value: T): void
    {
        var i = this.searchFor(key);
        this.items[ this.mappers[i].bit ] = value;
    }

    public iterate(iterator?: (currentIteration: T, index: number) => void): void
    {
        if (iterator)
            this.setIterator(iterator);
            
        if (!this.iterator)
            return;
            
        var len = this.mappers.length,
            bit = 0;

        for (var i = 0; i < len; i++)
        {
			bit = this.mappers[i].bit;
			this._currentIndex = bit;
			this._currentKey = this.mappers[i].key;
			this.iterator(this.items[bit], bit);
        }
    }

    private setIterator(iterator: (currentIteration: T, index: number) => void): void
    {
        this.iterator = iterator;
    }

    public exists(key: string): boolean | number
    {
        var len = this.mappers.length;
        for (var i = 0; i < len; i++)
        {
            if (key == this.mappers[i].key)
            {
                this._currentKey = key;
                this._currentIndex = i;
                return true;
            }
        }
        return false;
    }

    private searchFor(key: string): number
    {
        if (key == this._currentKey)
            return this._currentIndex;
            
        var len = this.mappers.length;
        for (var i = 0; i < len; i++)
        {
            if (key == this.mappers[i].key)
                return i;
        }

        throw new Error('Dictionary element "'+ key +'" not found');
    }
}