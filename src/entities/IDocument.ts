
interface IDocument
{
	id: string;
	updated: string;
	contents: any;
}