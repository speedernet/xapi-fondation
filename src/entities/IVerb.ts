///<reference path="../utils/langMap.ts"/>
///<reference path="../enums/baseVerb.ts"/>

interface IVerb
{
	id: BaseVerb;
	display?: LangMap;
}