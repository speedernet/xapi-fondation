///<reference path="IActor.ts"/>
///<reference path="impl/group.ts"/>
///<reference path="impl/statementRef.ts"/>
///<reference path="impl/activity.ts"/>

interface IContextActivities
{
	parent: Activity[];
	grouping: Activity[];
	category: Activity[];
	other: Activity[];
}

interface IContext
{
	/** @desc: UUID */
	registration: string;

	instructor: IActor;
	team: Group;
	contextActivities: IContextActivities;
	revision: string;
	platform: string;
	language: string;
	statement: StatementRef;

	/** @desc: object with variables uri in keys */
	extensions: Object;
}