///<reference path="../utils/langMap.ts"/>

interface IAttachment
{
	usageType: string;
	display: LangMap;
	description: LangMap;

	/** @desc: MIME-type */
	contentType: string;

	/** @desc: size (bytes) */
	length: number;

	sha2: string;
	fileUrl: string;
}