///<reference path="../enums/interactionType.ts"/>
///<reference path="../utils/langMap.ts"/>

interface IPotentialInteraction
{
	id: string;
	description: LangMap;
}

interface IInteractionDef
{
	type: string;
	interactionType: InteractionType;
	correctResponsePattern: any[];
	choices?: IPotentialInteraction[];
	scale?: IPotentialInteraction[];
	source?: IPotentialInteraction[];
	target?: IPotentialInteraction[];
	steps?: IPotentialInteraction[];
}