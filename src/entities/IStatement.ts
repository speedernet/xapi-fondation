///<reference path="IActor.ts"/>
///<reference path="IVerb.ts"/>
///<reference path="impl/Activity.ts"/>
///<reference path="IResult.ts"/>
///<reference path="impl/agent.ts"/>
///<reference path="IContext.ts"/>
///<reference path="IAttachment.ts"/>
///<reference path="impl/statementRef.ts"/>
///<reference path="impl/subStatement.ts"/>

interface IStatement
{
	/** @desc: UUID */
	id?: string;

	actor: IActor;
	verb: IVerb;
	object: Activity | StatementRef | SubStatement;
	result?: IResult;
	context?: IContext;

	authority?: Agent;
	attachments?: IAttachment[];

	/**
	 * @desc: date when the statement was issued
	 * @format: ISO 8601
	 */
	timestamp?: string;

	/**
	 * @desc: date when LRS stored the statement
	 * @format: ISO 8601
	 */
	stored?: string;

	/** @deprecated - not deprecated strictly speaking, but not recommended to set */
	version?: number;
}