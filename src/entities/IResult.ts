
interface IScore
{
	min: number;
	max: number;

	/** @desc: between -1 and 1 */
	scaled: number;

	/** @desc: between min and max */
	raw: number;
}

interface IResult
{
	/** @format: ISO 8601 */
	duration: string;
	completion: boolean;
	success: boolean;
	response: string;
	score: IScore;

	/** @desc: object with variables uri in keys */
	extensions: Object;
}