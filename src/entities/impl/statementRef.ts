///<reference path="../IObj.ts"/>

class StatementRef implements IObj
{
	/** @desc: UUID */
	public id: string;
	public objectType: ObjectType = new ObjectType(ObjectType.STATEMENT_REF);

	constructor() {}
}