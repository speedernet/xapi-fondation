///<reference path="../IActor.ts"/>
///<reference path="agent.ts"/>

class Group implements IActor
{
	public objectType: ObjectType = new ObjectType(ObjectType.GROUP);

	/** @desc: optional */
	public name: string = null;

	public member: Agent[] = new Array<Agent>();

	constructor() {}
}