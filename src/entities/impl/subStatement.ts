///<reference path="../IBaseObj.ts"/>
///<reference path="../IActor.ts"/>
///<reference path="../IVerb.ts"/>
///<reference path="activity.ts"/>

class SubStatement implements IStatement
{
	public objectType:ObjectType = new ObjectType(ObjectType.SUB_STATEMENT);
	public actor: IActor;
	public verb: IVerb;
	public object: Activity;
	public result: IResult = null;
	public context: IContext = null;
	public timestamp: string;

	constructor() {}
}