///<reference path="../IObj.ts"/>
///<reference path="../IInteractionDef.ts"/>

class Interaction implements IObj
{
	public id: string;
	public objectType: ObjectType = new ObjectType(ObjectType.ACTIVITY);
	public definition: IInteractionDef;

	constructor() {}
}