///<reference path="../../utils/langMap.ts"/>
///<reference path="../IObj.ts"/>

interface IActivityDef
{
	name: LangMap;
	desc: LangMap;

	/** @desc: iri */
	type: string;

	/** @desc: url to online desc */
	moreInfo: string;

	// any key/values
	interactions: Object;

	/** @desc: object with variables uri in keys */
	extensions: Object;
}

class Activity implements IObj
{
	public id: string;
	public definition: IActivityDef = null;

	/** @desc: optional */
	public objectType: ObjectType = new ObjectType(ObjectType.ACTIVITY);

	constructor() {}
}