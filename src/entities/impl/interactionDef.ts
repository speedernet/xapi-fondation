///<reference path="../IInteractionDef.ts"/>

/** @desc: for true/false, fill-in, long-fill-in, number, other */
class SimpleInteractionDef implements IInteractionDef
{
	public type: string;
	public interactionType: InteractionType;
	public correctResponsePattern: any[];

	constructor(type: InteractionType)
	{
		this.interactionType = type;
	}
}

class QCMInteractionDef implements IInteractionDef
{
	public type: string;
	public interactionType: InteractionType = new InteractionType(InteractionType.CHOICE);
	public correctResponsePattern: any[];
	public choices: IPotentialInteraction[] = new Array<IPotentialInteraction>();

	constructor() {}
}

class ScaleInteractionDef implements IInteractionDef
{
	public type: string;
	public interactionType: InteractionType = new InteractionType(InteractionType.LIKERT);
	public correctResponsePattern: any[];
	public scale: IPotentialInteraction[] = new Array<IPotentialInteraction>();

	constructor() {}
}

class MatchInteractionDef implements IInteractionDef
{
	public type: string;
	public interactionType: InteractionType = new InteractionType(InteractionType.MATCHING);
	public correctResponsePattern: any[];
	public source: IPotentialInteraction[] = new Array<IPotentialInteraction>();
	public target: IPotentialInteraction[] = new Array<IPotentialInteraction>();

	constructor() {}
}

class PerformanceInteractionDef implements IInteractionDef
{
	public type: string;
	public interactionType: InteractionType = new InteractionType(InteractionType.PERFORMANCE);
	public correctResponsePattern: any[];
	public steps: IPotentialInteraction[] = new Array<IPotentialInteraction>();

	constructor() {}
}

class SequencingInteractionDef implements IInteractionDef
{
	public type: string;
	public interactionType: InteractionType = new InteractionType(InteractionType.SEQUENCING);
	public correctResponsePattern: any[];
	public choices: IPotentialInteraction[] = new Array<IPotentialInteraction>();

	constructor() {}
}