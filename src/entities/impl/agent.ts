///<reference path="../IActor.ts"/>

class Agent implements IActor
{
	/** @desc: optional */
	public objectType: ObjectType = new ObjectType(ObjectType.AGENT);
	public name: string = null;

	constructor() {}
}