///<reference path="IBaseObj.ts"/>

interface IAccount
{
	homepage: string;
	name: string;
}

interface IActor extends IBaseObj
{
	name?: string;
	objectType?: ObjectType;

	/**
	 * @desc: ONLY one of these to use at a time - see children interfaces below
	 */
	mbox?: string; // starts with "mailto:", use Mbox.validate() to make sure
	mbox_sha1sum?: string;
	openid?: string;
	account?: IAccount;
}

interface IMboxActor extends IActor
{
	name?: string;
	objectType?: ObjectType;
	mbox: string;
}

interface IMboxShaActor extends IActor
{
	name?: string;
	objectType?: ObjectType;
	mbox_sha1sum: string;
}

interface IOpenidActor extends IActor
{
	name?: string;
	objectType?: ObjectType;
	openid: string;
}

interface IAccountActor extends IActor
{
	name?: string;
	objectType?: ObjectType;
	account: IAccount;
}