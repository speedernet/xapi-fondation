///<reference path="baseEnum.ts"/>

class ActivityType extends BaseEnum<string>
{
	// SCORM-like
	public static COURSE: string = "course";
	public static MODULE: string = "module";
	public static LESSON: string = "lesson";
	public static INTERACTION: string = "interaction";
	public static OBJECTIVE: string = "objective";
	public static ATTEMPT: string = "attempt";

	// xAPI bonuses
	public static FILE: string = "file";
	public static LINK: string = "link";
	public static MEDIA: string = "media";
	public static MEETING: string = "meeting";
	public static ASSESSMENT: string = "assessment";
	public static PERFORMANCE: string = "performance";
	public static PROFILE: string = "profile";
	public static QUESTION: string = "question";
	public static SIMULATION: string = "simulation";
}