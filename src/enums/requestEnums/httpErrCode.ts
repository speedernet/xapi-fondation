///<reference path="../baseEnum.ts"/>

class RestErrCode extends BaseEnum<number>
{
	public static 400: number = 400; // Bad Request
	public static 401: number = 401; // Unauthorized
	public static 403: number = 403; // Forbidden
	public static 404: number = 404; // Not Found
	public static 409: number = 409; // Conflict
	public static 412: number = 412; // Precondition Failed
	public static 413: number = 413; // Request Entity Too Large
	public static 500: number = 500; // Internal Server Error
}