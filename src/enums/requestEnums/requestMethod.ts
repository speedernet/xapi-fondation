///<reference path="../baseEnum.ts"/>

class RequestMethod extends BaseEnum<string>
{
	public static PUT: string = "PUT";
	public static POST: string = "POST";
	public static GET: string = "GET";
	public static DELETE: string = "DELETE";
}