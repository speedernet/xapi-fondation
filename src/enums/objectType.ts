///<reference path="baseEnum.ts"/>

class ObjectType extends BaseEnum<string>
{
	public static GROUP: string = 'Group';
	public static AGENT: string = "Agent";
	public static ACTIVITY: string = "Activity";
	public static STATEMENT: string = "Statement";
	public static STATEMENT_REF: string = "StatementRef";
	public static SUB_STATEMENT: string = "SubStatement";
}