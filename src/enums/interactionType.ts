///<reference path="baseEnum.ts"/>

class InteractionType extends BaseEnum<string>
{
	public static TRUE_FALSE: string = "true-false";
	public static CHOICE: string = "choice"; // multiple-choice question (checkboxes)
	public static FILL_IN: string = "fill-in"; // blanked-out text
	public static LONG_FILL_IN: string = "long-fill-in"; // any text
	public static LIKERT: string = "likert"; // scale elements
	public static MATCHING: string = "matching"; // association of elements
	public static PERFORMANCE: string = "performance"; // procedure
	public static SEQUENCING: string = "sequencing"; // ranking
	public static NUMERIC: string = "numeric";
	public static OTHER: string = "other";
}