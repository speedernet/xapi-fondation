///<reference path="baseEnum.ts"/>

class BaseVerb extends BaseEnum<string>
{
	/** @desc: simple stuff */
	public static ATTEMPTED: string = "http://adlnet.gov/expapi/verbs/attempted";
	public static EXPERIENCED: string = "http://adlnet.gov/expapi/verbs/experienced";
	public static PROGRESSED: string = "http://adlnet.gov/expapi/verbs/progressed";
	public static INTERACTED: string = "http://adlnet.gov/expapi/verbs/interacted";
	public static COMPLETED: string = "http://adlnet.gov/expapi/verbs/completed";

	/** @desc: session management */
	public static INITIALIZED: string = "http://adlnet.gov/expapi/verbs/initialized";
	public static RESUMED: string = "http://adlnet.gov/expapi/verbs/resumed";
	public static SUSPENDED: string = "http://adlnet.gov/expapi/verbs/suspended";
	public static EXITED: string = "http://adlnet.gov/expapi/verbs/exited";
	public static TERMINATED: string = "http://adlnet.gov/expapi/verbs/terminated";

	/** @desc: navigation */
	public static LAUNCHED: string = "http://adlnet.gov/expapi/verbs/launched";

	/** @desc: quiz */
	public static RESPONDED: string = "http://adlnet.gov/expapi/verbs/responded";
	public static PASSED: string = "http://adlnet.gov/expapi/verbs/passed";
	public static SCORED: string = "http://adlnet.gov/expapi/verbs/scored";
	public static FAILED: string = "http://adlnet.gov/expapi/verbs/failed";

	/** @desc: social */
	public static ASKED: string = "http://adlnet.gov/expapi/verbs/asked";
	public static COMMENTED: string = "http://adlnet.gov/expapi/verbs/commented";
	public static ANSWERED: string = "http://adlnet.gov/expapi/verbs/aswered";
	public static SHARED: string = "http://adlnet.gov/expapi/verbs/shared";

	/** @desc: blended */
	public static REGISTERED: string = "http://adlnet.gov/expapi/verbs/registered";
	public static ATTENDED: string = "http://adlnet.gov/expapi/verbs/attended";

	/** @desc: skills */
	public static MASTERED: string = "http://adlnet.gov/expapi/verbs/mastered";

	/** @desc: other */
	public static IMPORTED: string = "http://adlnet.gov/expapi/verbs/imported";
	public static PREFERRED: string = "http://adlnet.gov/expapi/verbs/preferred";
	public static VOIDED: string = "http://adlnet.gov/expapi/verbs/voided";
}