///<reference path="../enums/requestEnums/requestMethod.ts"/>
///<reference path="../entities/IStatement.ts"/>

/** @desc: abstract */
class BaseRequester // note: should be abstract
{
	public endpoint: string; // service uri
	protected socket: XMLHttpRequest;

	constructor(uri?: string)
	{
		if (uri)
			this.endpoint = uri;

		this.socket = new XMLHttpRequest();
		this.socket.onreadystatechange = (event: Event) => this.asyncReturn(event);
	}

	public put(stuff: IStatement): void
	{
		this.request(RequestMethod.PUT, stuff);
	}

	public post(stuff: IStatement | IStatement[]): void
	{
		this.request(RequestMethod.POST, stuff);
	}

	public get(search?: IStatement): void
	{
		this.request(RequestMethod.GET, search);
	}

	private request(mode: string, params?: IStatement | IStatement[]): void
	{
		if (!this.endpoint)
			throw new Error('No webservice url set');

		this.socket.open(mode, this.endpoint, true);
		this.socket.send(params);
	}

	private asyncReturn(event: Event): void
	{
		if (4 == this.socket.readyState && this.socket.response)
		{
			if (200 == this.socket.status)
				this.result(this.socket.response);
			else
				this.handleSocketError(this.socket.status);
		}
	}

	private result(ajaxResponse: string): void
	{
		// TODO for request treatment, add your own callbacks
		var result: IStatement | IStatement[] = JSON.parse(ajaxResponse);
		console.error('Abstract method, implement me');
	}

	private handleSocketError(errCode: number): void
	{
		console.error('Abstract method, implement me');
	}
}