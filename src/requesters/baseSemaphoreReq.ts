///<reference path="baseReq.ts"/>

/** @desc: abstract */
class BaseSemaphoreRequester extends BaseRequester
{
	// TODO: you must update ETag on get result
	/** @desc: a hash received from LRS to identify data version */
	private ETag: string = '';

	public put(stuff: IStatement): void
	{
		var oldTag: string = this.ETag;
		this.get(stuff);

		if (oldTag == this.ETag)
			super.put(stuff);
	}

	public post(stuff: IStatement | IStatement[]): void
	{
		console.warn('No transactional control on post method');
		super.post(stuff);
	}
}