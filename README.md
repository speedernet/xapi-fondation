# xAPI TypeScript - README #

### What is this repository for? ###

* This is a TypeScript draft of the [xAPI norm](https://github.com/adlnet/xAPI-Spec/blob/master/xAPI.md) (also called TinCan) in which I try to translate its logical objects in classes and interfaces for further developments.

* I aim at simplifying the json automated writing and reading to make sure my data are always norm-proof.

* This is currently untested, any feedback and improvements are welcome!

* Please note that the norm is only about data formatting, and not data treatment. Hence the absence of implementation of some classes I reckoned not needed.

### How do I get set up? ###

* If you just need to use its functionalities without asking anything, you can get the main.js file from the build folder (or its minified version), even though you shouldn't be interested in that part at all ^^

* For the data formalism part, this is currently a set of basic objects to use, no set up required except copying the repo.

* The script's entry point is the src/main.ts file

* The tsBuild.sh file is a bash script that will allow you to build the TypeScript files into Javascript once you're all set and want to test. You will need to install the **tsc** command first (visit [http://www.typescriptlang.org](http://www.typescriptlang.org)), and **uglifyjs** ([https://github.com/mishoo/UglifyJS2](https://github.com/mishoo/UglifyJS2)) if you want to minify it.

### Who do I talk to? ###

* The main dev of the project! :)